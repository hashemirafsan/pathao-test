<?php declare(strict_types=1);

/**
 * Class LRUCache
 */
class LRUCache
{
    /**
     * @var array $cache
     */
    protected array $cache = [];

    /**
     * @var int $size
     */
    protected int $size;

    /**
     * @param int $size
     * @return LRUCache
     */
    public function setSize(int $size): self
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @return array
     */
    public function getCache(): array
    {
        return $this->cache;
    }

    /**
     * @return bool
     */
    private function isCacheFull(): bool 
    {
        return isset($this->cache[$this->size - 1]);
    }

    /**
     * @param int $item
     */
    public function refer(int $item): void
    {
        if ($this->isCacheFull()) {
            if (in_array($item, $this->cache, true)) {
                $this->cache = array_diff($this->cache, [$item]);
            } else {
                array_pop($this->cache);
            }
        }

        array_unshift($this->cache, $item);
    }
}

$size = 4;

$lruCache = (new LRUCache());

$lruCache->setSize($size);

$lruCache->refer(1);
$lruCache->refer(2);
$lruCache->refer(3);
$lruCache->refer(1);
$lruCache->refer(4);
$lruCache->refer(5);


echo implode(',', $lruCache->getCache());