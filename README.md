# Pathao Test Documentation

Installation
---
- Application have dockerize version, So we need to go two step to build the application
- Before run build command, try to check `.env` file in root directory. if any expose port conflicted
  on your system then change it.
- Run `docker-compose up --build -d` 
- After successfull build, we need to go application container for run migration
- Run `docker-compose exec app sh`
- Run `php artisan migrate`
- All set, application is running on [http://localhost:8034](http://localhost:8034)

Application Structure
---
- Controllers are `src/app/Http/Controller/API/V1` here.
- Models are `src/app/Models` here.
- Service are `src/app/Http/Services` here.
- Repositories are `src/app/Repositories` here.
- Routes are `src/routes/api.v1.php` here.
- Testcases are `src/tests` here.

Testing
---
- Run `docker-compose exec app sh`
- Run `./vendor/bin/phpunit`

Postman Collection
---
- Published Link [https://documenter.getpostman.com/view/391183/TzJsfdFE](https://documenter.getpostman.com/view/391183/TzJsfdFE)
- Public Link [https://www.getpostman.com/collections/4f2a57a80dfe64213405](https://www.getpostman.com/collections/4f2a57a80dfe64213405)

Implement LRU Caching using PHP
---
- Implementation file `LRUCache.php` will be available in root directory.

Conclusion
---
Both task i'm trying to give my best afford, but there will be lots of improvement. If any kind of issues/feedback arise, please let me know to implement. 


