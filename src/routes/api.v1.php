<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| API V1 Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Http\Controllers\API\V1\AuthorController;

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/authors', 'AuthorController@list');
$router->group(['prefix' => 'author'], function ($router) {
    $router->post('/', 'AuthorController@store');
    $router->get('/{id}', 'AuthorController@show');
    $router->put('/{id}', 'AuthorController@update');
    $router->delete('/{id}', 'AuthorController@destroy');
});

$router->get('/books', 'BookController@list');
$router->group(['prefix' => 'book'], function ($router) {
    $router->post('/', 'BookController@store');
    $router->get('/{id}', 'BookController@show');
    $router->put('/{id}', 'BookController@update');
    $router->delete('/{id}', 'BookController@destroy');
});
