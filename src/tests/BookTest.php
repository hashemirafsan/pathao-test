<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class BookTest extends TestCase
{
    protected $authors = [];

    protected function setUp(): void
    {
        parent::setUp();
        $this->authors = \App\Models\Author::select('id')->get()->pluck('id');
    }

    public function testStoreFirstBook()
    {
        $faker = \Faker\Factory::create();
        $randAuthor = \Illuminate\Support\Arr::random($this->authors->toArray(), 2);

        $data = [
            'title' => $faker->unique()->name('kkckskcmsckdskmcksdkjcs'),
            'total_pages' => $faker->randomDigit(),
            'published_at' => $faker->date('Y-m-d'),
            'authors' => $randAuthor
        ];

        $response = $this->json('POST', 'api/v1/book', $data);

        $body = json_decode($response->response->getContent());

        $data['id'] = $body->book->id;

        $this->seeStatusCode(201);

        return $data;
    }

    public function testStoreSecondBook()
    {
        $faker = \Faker\Factory::create();

        $randAuthor = \Illuminate\Support\Arr::random($this->authors->toArray(), 2);

        $data = [
            'title' => $faker->unique()->name('scsdcujuybbhbacjknncppauuyuehybcvtvaga'),
            'total_pages' => $faker->randomDigit(),
            'published_at' => $faker->date('Y-m-d'),
            'authors' => $randAuthor
        ];

        $response = $this->json('POST', 'api/v1/book', $data);
        $body = json_decode($response->response->getContent());

        $data['id'] = $body->book->id;

        $this->seeStatusCode(201);

        return $data;
    }

    public function testStoreThirdBook()
    {
        $faker = \Faker\Factory::create();

        $randAuthor = \Illuminate\Support\Arr::random($this->authors->toArray(), 2);

        $data = [
            'title' => $faker->unique()->name('male'),
            'total_pages' => $faker->randomDigit(),
            'published_at' => $faker->date('Y-m-d'),
            'authors' => $randAuthor
        ];

        $response = $this->json('POST', 'api/v1/book', $data);

        $body = json_decode($response->response->getContent());

        $data['id'] = $body->book->id;

        $this->seeStatusCode(201);

        return $data;
    }

    /**
     * @depends testStoreFirstBook
     */
    public function testFindFirstBook($data)
    {
        $this->get('/api/v1/book/' . $data['id']);

        $this->seeStatusCode(200);
        $this->seeJson([
            'code' => 200,
            'status' => \App\Enums\ResponseEnum::FOUND_SUCCESSFULLY
        ]);
    }

    /**
     * @depends testStoreSecondBook
     */
    public function testUpdateTitleAndTotalPageSecondBook($data)
    {
        $faker = \Faker\Factory::create();
        $data['title'] = $faker->unique()->name('male');
        $data['total_page'] = $faker->randomDigit;

        $response = $this->json('PUT', 'api/v1/book/' . $data['id'], $data);

        $this->seeStatusCode(200);

        $this->seeJson([
            'code' => 200,
            'status' => \App\Enums\ResponseEnum::SUCCESSFULLY_UPDATED,
        ]);
    }

    /**
     * @depends testStoreThirdBook
     */
    public function testUpdateAuthorsThirdBook($data)
    {
        $faker = \Faker\Factory::create();
        $randAuthor = \Illuminate\Support\Arr::random($this->authors->toArray(), 2);

        $onlyData['authors'] = $randAuthor;

        $response = $this->json('PUT', 'api/v1/book/' . $data['id'], $onlyData);

        $this->seeStatusCode(200);

        $this->seeJson([
            'code' => 200,
            'status' => \App\Enums\ResponseEnum::SUCCESSFULLY_UPDATED,
        ]);
    }

    /**
     * @depends testStoreFirstBook
     */
    public function testDuplicateBookName($data)
    {
        $response = $this->json('POST', 'api/v1/book', $data);

        $this->seeStatusCode(422);
        $this->seeJson([
            'code' => 422,
            'status' => \App\Enums\ResponseEnum::VALIDATION_FAILED
        ]);
    }

    /**
     * @depends testStoreSecondBook
     */
    public function testDeleteSecondBook($data)
    {
        $this->delete('api/v1/book/' . $data['id']);

        $this->seeStatusCode(200);
        $this->seeJson([
            'code' => 200,
            'status' => \App\Enums\ResponseEnum::SUCCESSFULLY_DESTROYED
        ]);
    }

    /**
     * @depends testStoreSecondBook
     */
    public function testDeleteSecondBookAgain($data)
    {
        $this->delete('api/v1/book/' . $data['id']);

        $this->seeStatusCode(400);
        $this->seeJson([
            'code' => 400,
            'status' => \App\Enums\ResponseEnum::FAILED_TO_DESTROY
        ]);
    }

    /**
     * @depends testStoreSecondBook
     */
    public function testFindSecondBook($data)
    {
        $this->get('/api/v1/book/' . $data['id']);
        $this->seeStatusCode(404);
        $this->seeJson([
            'code' => 404,
            'status' => \App\Enums\ResponseEnum::NO_DATA_FOUND
        ]);
    }
}
