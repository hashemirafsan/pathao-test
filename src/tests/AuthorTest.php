<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class AuthorTest extends TestCase
{
    public function testStoreFirstAuthor()
    {
        $faker = \Faker\Factory::create();

        $data = [
            'name' => $faker->unique()->name('male')
        ];

        $response = $this->json('POST', '/api/v1/author', $data);

        $this->seeStatusCode(201);

        $body = json_decode($response->response->getContent());

        $data['id'] = $body->author->id;

        $this->seeJsonEquals([
            'code' => 201,
            'status' => \App\Enums\ResponseEnum::SUCCESSFULLY_STORED,
            'author' => $data
        ]);

        return $data;

    }

    public function testStoreSecondAuthor()
    {
        $faker = \Faker\Factory::create();

        $data = [
            'name' => $faker->unique()->name('female')
        ];

        $response = $this->json('POST', '/api/v1/author', $data);

        $this->seeStatusCode(201);

        $body = json_decode($response->response->getContent());

        $data['id'] = $body->author->id;

        $this->seeJsonEquals([
            'code' => 201,
            'status' => \App\Enums\ResponseEnum::SUCCESSFULLY_STORED,
            'author' => $data
        ]);

        return $data;

    }

    public function testStoreThirdAuthor()
    {
        $faker = \Faker\Factory::create();

        $data = [
            'name' => $faker->unique()->name('male')
        ];

        $response = $this->json('POST', '/api/v1/author', $data);

        $this->seeStatusCode(201);

        $body = json_decode($response->response->getContent());

        $data['id'] = $body->author->id;

        $this->seeJsonEquals([
            'code' => 201,
            'status' => \App\Enums\ResponseEnum::SUCCESSFULLY_STORED,
            'author' => $data
        ]);

        return $data;

    }

    /**
     * @depends testStoreFirstAuthor
     */
    public function testFindAuthor($data)
    {
        $data['books'] = [];
        $response = $this->get('/api/v1/author/' . $data['id']);

        $this->seeStatusCode(200);

        $this->seeJsonEquals([
            'code' => 200,
            'status' => \App\Enums\ResponseEnum::FOUND_SUCCESSFULLY,
            'author' => $data
        ]);

        return $data;
    }

    /**
     * @depends testStoreSecondAuthor
     */
    public function testUpdateAuthor($data)
    {
        $faker = \Faker\Factory::create();

        $data['name'] = $faker->unique()->name;

        $response = $this->json('PUT', '/api/v1/author/' . $data['id'], $data);

        $this->seeStatusCode(200);

        $this->seeJsonEquals([
            'code' => 200,
            'status' => \App\Enums\ResponseEnum::SUCCESSFULLY_UPDATED,
            'author' => $data
        ]);

        return $data;
    }

    /**
     * @depends testStoreThirdAuthor
     */
    public function testDeleteAuthor($data)
    {
        $response = $this->delete( '/api/v1/author/' . $data['id']);

        $this->seeStatusCode(200);

        $this->seeJsonEquals([
            'code' => 200,
            'status' => \App\Enums\ResponseEnum::SUCCESSFULLY_DESTROYED,
        ]);
    }

    /**
     * @depends testStoreThirdAuthor
     */
    public function testFindThirdAuthor($data)
    {
        $response = $this->get( '/api/v1/author/' . $data['id']);

        $this->seeStatusCode(404);

        $this->seeJson([
            'code' => 404,
            'status' => \App\Enums\ResponseEnum::NO_DATA_FOUND,
        ]);
    }
}
