<?php

namespace App\Http\Controllers\API\V1;

use App\Enums\ResponseEnum;
use App\Http\Controllers\Controller;
use App\Http\Services\BookService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

/**
 * Class BookController
 * @package App\Http\Controllers\API\V1
 */
class BookController extends Controller
{
    /**
     * @var BookService $bookService
     */
    protected BookService $bookService;

    /**
     * BookController constructor.
     * @param BookService $bookService
     */
    public function __construct(BookService $bookService)
    {
        $this->bookService = $bookService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request): JsonResponse
    {
        $data = $this->bookService->search($request->only(['q']));

        return response()->json([
            'code' => Response::HTTP_OK,
            'status' => ResponseEnum::SUCCESSFULLY_RETRIEVED,
            'data' => $data
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $response = [
            'code' => Response::HTTP_CREATED,
            'status' => ResponseEnum::SUCCESSFULLY_STORED
        ];

        $validator = Validator::make($request->all(), [
            'title' => ['required', 'string', 'max:100', 'regex:/^[A-Za-z0-9 .!?(-)]+$/', 'unique:books,title'],
            'total_pages' => ['required', 'numeric'],
            'published_at' => ['required', 'date_format:Y-m-d'],
            'authors' => ['required', 'array'],
            'authors.*' => ['required', 'distinct', 'numeric', 'exists:authors,id']
        ]);

        if ($validator->fails()) {
            $response = array_merge($response, [
                'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'status' => ResponseEnum::VALIDATION_FAILED,
                'errors' => $validator->errors()
            ]);
        } else {
            $book = $this->bookService->store($validator->validate());

            if (! $book) {
                $response = array_merge($response, [
                    'code' => Response::HTTP_BAD_REQUEST,
                    'status' => ResponseEnum::FAILED_TO_STORE
                ]);
            } else {
                $response['book'] = $book->toArray();
            }
        }

        return response()->json($response, $response['code']);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $response = [
            'code' => Response::HTTP_OK,
            'status' => ResponseEnum::FOUND_SUCCESSFULLY
        ];

        $book = $this->bookService->show($id);

        $response['book'] = $book;

        if (! $book) {
            $response['code'] = Response::HTTP_NOT_FOUND;
            $response['status'] = ResponseEnum::NO_DATA_FOUND;
        }

        return response()->json($response, $response['code']);
    }

    /**
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(int $id, Request $request): JsonResponse
    {
        $response = [
            'code' => Response::HTTP_OK,
            'status' => ResponseEnum::SUCCESSFULLY_UPDATED
        ];

        $validator = Validator::make($request->all(), [
            'title' => ['nullable', 'string', 'max:100', 'regex:/^[A-Za-z0-9 .!?(-)]+$/', 'unique:books,title,' . $id],
            'total_pages' => ['nullable', 'numeric'],
            'published_at' => ['nullable', 'date_format:Y-m-d'],
            'authors' => ['nullable', 'array'],
            'authors.*' => ['required', 'distinct', 'numeric', 'exists:authors,id']
        ]);

        if ($validator->fails()) {
            $response = array_merge($response, [
                'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'status' => ResponseEnum::VALIDATION_FAILED,
                'errors' => $validator->errors()
            ]);
        } else {
            $author = $this->bookService->update($id, $validator->validate());

            if (! $author) {
                $response = array_merge($response, [
                    'code' => Response::HTTP_BAD_REQUEST,
                    'status' => ResponseEnum::FAILED_TO_UPDATE
                ]);
            } else {
                $response['book'] = $author->toArray();
            }
        }

        return response()->json($response, $response['code']);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $response = [
            'code' => Response::HTTP_OK,
            'status' => ResponseEnum::SUCCESSFULLY_DESTROYED
        ];

        $isDestroyed = $this->bookService->destroy($id);

        if (! $isDestroyed) {
            $response['code'] = Response::HTTP_BAD_REQUEST;
            $response['status'] = ResponseEnum::FAILED_TO_DESTROY;
        }

        return response()->json($response, $response['code']);
    }
}
