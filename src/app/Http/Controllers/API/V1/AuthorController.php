<?php declare(strict_types=1);


namespace App\Http\Controllers\API\V1;

use App\Enums\ResponseEnum;
use App\Http\Controllers\Controller;
use App\Http\Services\AuthorService;
use Illuminate\Http\{JsonResponse, Request, Response};
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

/**
 * Class AuthorController
 * @package App\Http\Controllers\API\V1
 */
class AuthorController extends Controller
{
    /**
     * @var AuthorService $authorService
     */
    protected AuthorService $authorService;

    /**
     * AuthorController constructor.
     * @param AuthorService $authorService
     */
    public function __construct(AuthorService $authorService)
    {
        $this->authorService = $authorService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request): JsonResponse
    {
        $data = $this->authorService->search($request->only(['q']));

        return response()->json([
            'code' => Response::HTTP_OK,
            'status' => ResponseEnum::SUCCESSFULLY_RETRIEVED,
            'data' => $data
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $response = [
            'code' => Response::HTTP_CREATED,
            'status' => ResponseEnum::SUCCESSFULLY_STORED
        ];

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'min:3', 'max:50', 'regex:/^[A-Za-z .-]+$/', 'unique:authors,name']
        ]);

        if ($validator->fails()) {
            $response = array_merge($response, [
                'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'status' => ResponseEnum::VALIDATION_FAILED,
                'errors' => $validator->errors()
            ]);
        } else {
            $author = $this->authorService->store($validator->validate());

            if (! $author) {
                $response = array_merge($response, [
                    'code' => Response::HTTP_BAD_REQUEST,
                    'status' => ResponseEnum::FAILED_TO_STORE
                ]);
            } else {
                $response['author'] = $author->toArray();
            }
        }

        return response()->json($response, $response['code']);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $response = [
            'code' => Response::HTTP_OK,
            'status' => ResponseEnum::FOUND_SUCCESSFULLY
        ];

        $author = $this->authorService->show($id);

        $response['author'] = $author;

        if (! $author) {
            $response['code'] = Response::HTTP_NOT_FOUND;
            $response['status'] = ResponseEnum::NO_DATA_FOUND;
        }

        return response()->json($response, $response['code']);
    }

    /**
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(int $id, Request $request): JsonResponse
    {
        $response = [
            'code' => Response::HTTP_OK,
            'status' => ResponseEnum::SUCCESSFULLY_UPDATED
        ];

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'min:3', 'max:50', 'regex:/^[A-Za-z .-]+$/', 'unique:authors,name,' . $id]
        ]);

        if ($validator->fails()) {
            $response = array_merge($response, [
                'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'status' => ResponseEnum::VALIDATION_FAILED,
                'errors' => $validator->errors()
            ]);
        } else {
            $author = $this->authorService->update($id, $validator->validate());

            if (! $author) {
                $response = array_merge($response, [
                    'code' => Response::HTTP_BAD_REQUEST,
                    'status' => ResponseEnum::FAILED_TO_UPDATE
                ]);
            } else {
                $response['author'] = $author->toArray();
            }
        }

        return response()->json($response, $response['code']);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $response = [
            'code' => Response::HTTP_OK,
            'status' => ResponseEnum::SUCCESSFULLY_DESTROYED
        ];

        $isDestroyed = $this->authorService->destroy($id);

        if (! $isDestroyed) {
            $response['code'] = Response::HTTP_BAD_REQUEST;
            $response['status'] = ResponseEnum::FAILED_TO_DESTROY;
        }

        return response()->json($response, $response['code']);
    }
}
