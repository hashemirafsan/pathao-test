<?php declare(strict_types=1);


namespace App\Http\Services;

use App\Models\Author;
use App\Repositories\Interfaces\AuthorRepositoryInterface;
use Illuminate\Support\Facades\DB;

/**
 * Class AuthorService
 * @package App\Http\Services
 */
class AuthorService
{
    /**
     * @var AuthorRepositoryInterface $authorRepository
     */
    protected AuthorRepositoryInterface $authorRepository;

    /**
     * @var BookAuthorService $bookAuthorService
     */
    protected BookAuthorService $bookAuthorService;

    /**
     * AuthorService constructor.
     * @param AuthorRepositoryInterface $authorRepository
     * @param BookAuthorService $bookAuthorService
     */
    public function __construct(AuthorRepositoryInterface $authorRepository, BookAuthorService $bookAuthorService)
    {
        $this->authorRepository = $authorRepository;
        $this->bookAuthorService = $bookAuthorService;
    }

    /**
     * @param array $conditions
     * @return object
     */
    public function search(array $conditions = []): object
    {
        return $this->authorRepository->list($conditions);
    }

    /**
     * @param array $data
     * @return object|null
     */
    public function store(array $data): ?object
    {
        return $this->authorRepository->store($data);
    }

    /**
     * @param int $id
     * @return object|null
     */
    public function show(int $id): ?object
    {
        return $this->authorRepository->show($id);
    }

    /**
     * @param int $id
     * @param array $data
     * @return object|null
     */
    public function update(int $id, array $data): ?object
    {
        return $this->authorRepository->update($id, $data);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function destroy(int $id): bool
    {
        DB::beginTransaction();

        try {
            $this->bookAuthorService->detachByModelName($id, Author::class);
            $this->authorRepository->destroy($id);
            DB::commit();

            return true;
        } catch (\Exception $exception) {
            DB::rollBack();

            return false;
        }
    }
}
