<?php declare(strict_types=1);

namespace App\Http\Services;

use App\Models\Book;
use App\Repositories\Interfaces\BookRepositoryInterface;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

/**
 * Class BookService
 * @package App\Http\Services
 */
class BookService
{
    /**
     * @var BookRepositoryInterface $bookRepository
     */
    protected BookRepositoryInterface $bookRepository;

    /**
     * @var BookAuthorService $bookAuthorService
     */
    protected BookAuthorService $bookAuthorService;

    /**
     * BookService constructor.
     * @param BookRepositoryInterface $bookRepository
     * @param BookAuthorService $bookAuthorService
     */
    public function __construct(BookRepositoryInterface $bookRepository, BookAuthorService $bookAuthorService)
    {
        $this->bookRepository = $bookRepository;
        $this->bookAuthorService = $bookAuthorService;
    }

    /**
     * @param array $conditions
     * @return object
     */
    public function search(array $conditions = []): object
    {
        return $this->bookRepository->list($conditions);
    }

    /**
     * @param array $data
     * @return object|null
     */
    public function store(array $data): ?object
    {
        DB::beginTransaction();

        try {
            $book = $this->bookRepository->store(Arr::only($data, ['title', 'total_pages', 'published_at']));

            if (! is_null($book)) {
                $this->bookAuthorService->attachByRelation($book, 'authors', $data['authors']);
            }
            DB::commit();

            return $book->load('authors');
        } catch (\Exception $exception) {
            DB::rollBack();

            return null;
        }
    }

    /**
     * @param int $id
     * @return object|null
     */
    public function show(int $id): ?object
    {
        return $this->bookRepository->show($id);
    }

    /**
     * @param int $id
     * @param array $data
     * @return object|null
     */
    public function update(int $id, array $data): ?object
    {
        DB::beginTransaction();

        try {
            $book = $this->bookRepository->update($id, Arr::only($data, ['title', 'total_pages', 'published_at']));

            if ((! is_null($book)) && array_key_exists('authors', $data)) {
                $this->bookAuthorService->syncByRelation($book, 'authors', $data['authors']);
            }
            DB::commit();

            return $book->load('authors');
        } catch (\Exception $exception) {
            DB::rollBack();
            return null;
        }
    }

    /**
     * @param int $id
     * @return bool
     */
    public function destroy(int $id): bool
    {
        DB::beginTransaction();

        try {
            $this->bookAuthorService->detachByModelName($id, Book::class);
            $isDestroy = $this->bookRepository->destroy($id);
            DB::commit();

            return $isDestroy;
        } catch (\Exception $exception) {
            DB::rollBack();

            return false;
        }
    }
}
