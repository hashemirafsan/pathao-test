<?php declare(strict_types=1);

namespace App\Http\Services;

use App\Repositories\Interfaces\BookAuthorRepositoryInterface;

/**
 * Class BookAuthorService
 * @package App\Http\Services
 */
class BookAuthorService
{
    /**
     * @var BookAuthorRepositoryInterface $bookAuthorRepository
     */
    protected BookAuthorRepositoryInterface $bookAuthorRepository;

    /**
     * BookAuthorService constructor.
     * @param BookAuthorRepositoryInterface $bookAuthorRepository
     */
    public function __construct(BookAuthorRepositoryInterface $bookAuthorRepository)
    {
        $this->bookAuthorRepository = $bookAuthorRepository;
    }

    /**
     * @param object $model
     * @param string $relation
     * @param array $attachIds
     * @return mixed
     */
    public function attachByRelation(object $model, string $relation, array $attachIds)
    {
        return $model->{$relation}()->attach($attachIds);
    }

    /**
     * @param object $model
     * @param string $relation
     * @param array $syncIds
     * @return mixed
     */
    public function syncByRelation(object $model, string $relation, array $syncIds)
    {
        return $model->{$relation}()->sync($syncIds);
    }

    /**
     * When you have not model instance but you want to delete
     * pivot table data then use this method
     *
     * @param int $id
     * @param string $model
     * @return bool
     */
    public function detachByModelName(int $id, string $model): bool
    {
        if (! class_exists($model)) return false;

        $pivotColumn = strtolower(last(explode('\\', $model))) . '_id';

        return $this->bookAuthorRepository->detach($id, $pivotColumn);
    }
}
