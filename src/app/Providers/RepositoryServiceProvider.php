<?php


namespace App\Providers;

use App\Repositories\{AuthorRepository, BookAuthorRepository, BookRepository};
use App\Repositories\Interfaces\{AuthorRepositoryInterface, BookAuthorRepositoryInterface, BookRepositoryInterface};
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AuthorRepositoryInterface::class, AuthorRepository::class);
        $this->app->bind(BookRepositoryInterface::class, BookRepository::class);
        $this->app->bind(BookAuthorRepositoryInterface::class, BookAuthorRepository::class);
    }
}
