<?php declare(strict_types=1);

namespace App\Repositories;

use App\Models\Author;
use App\Repositories\Interfaces\AuthorRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

/**
 * Class AuthorRepository
 * @package App\Repositories
 */
class AuthorRepository implements AuthorRepositoryInterface
{
    /**
     * @var Author $model
     */
    protected Author $model;

    /**
     * AuthorRepository constructor.
     * @param Author $model
     */
    public function __construct(Author $model)
    {
        $this->model = $model;
    }

    /**
     * @return Model
     */
    public function model(): Model
    {
        return $this->model;
    }

    /**
     * @param int $id
     * @param bool $withs
     * @return object|null
     */
    public function show(int $id, bool $withs = true): ?object
    {
        try {
            $query = $this->model()->newQuery();

            if ($withs) {
                $query->with(['books']);
            }

            return $query->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            return null;
        }
    }

    /**
     * @param array $data
     * @return object|null
     */
    public function store(array $data): object
    {
        return $this->model()->newQuery()->create($data);
    }

    /**
     * @param int $id
     * @param array $data
     * @return object|null
     */
    public function update(int $id, array $data): ?object
    {
        $this->model()->newQuery()->where('id', $id)->update($data);
        return $this->show($id, false);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function destroy(int $id)
    {
        return $this->model()->newQuery()->where('id', $id)->delete();

    }

    /**
     * @param array $conditions
     * @return object
     */
    public function list(array $conditions = []): object
    {
        $query = $this->model()->newQuery()->with(['books']);

        if (array_key_exists('q', $conditions)) {
            $query->where(DB::raw('LOWER(name)'), 'LIKE', '%' . $conditions['q'] . '%');
        }

        return $query->simplePaginate(20);
    }
}
