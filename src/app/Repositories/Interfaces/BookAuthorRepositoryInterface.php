<?php


namespace App\Repositories\Interfaces;


use Illuminate\Database\Eloquent\Model;

interface BookAuthorRepositoryInterface
{
    public function model(): Model;

    public function detach(int $id, string $column);
}
