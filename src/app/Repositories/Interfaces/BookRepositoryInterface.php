<?php


namespace App\Repositories\Interfaces;

/**
 * Interface AuthorRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface BookRepositoryInterface extends RepositoryInterface
{
    public function list(array $conditions = []): object;
}
