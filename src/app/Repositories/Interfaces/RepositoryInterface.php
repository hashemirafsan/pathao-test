<?php declare(strict_types=1);

namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Model;

/**
 * Interface RepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface RepositoryInterface
{
    public function model(): Model;

    public function show(int $id, bool $withs = true): ?object;

    public function store(array $data): object;

    public function update(int $id, array $data): ?object;

    public function destroy(int $id);
}
