<?php declare(strict_types=1);


namespace App\Repositories;


use App\Models\BookAuthor;
use App\Repositories\Interfaces\BookAuthorRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class BookAuthorRepository implements BookAuthorRepositoryInterface
{
    /**
     * @var BookAuthor $model
     */
    protected BookAuthor $model;

    /**
     * BookAuthorRepository constructor.
     * @param BookAuthor $model
     */
    public function __construct(BookAuthor $model)
    {
        $this->model = $model;
    }

    /**
     * @return Model
     */
    public function model(): Model
    {
        return $this->model;
    }


    /**
     * @param int $id
     * @param string $column
     * @return bool
     */
    public function detach(int $id, string $column)
    {
        return $this->model()->newQuery()->where($column, $id)->delete();
    }
}
