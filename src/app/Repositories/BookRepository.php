<?php declare(strict_types=1);

namespace App\Repositories;

use App\Models\Book;
use App\Repositories\Interfaces\BookRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

/**
 * Class BookRepository
 * @package App\Repositories
 */
class BookRepository implements BookRepositoryInterface
{
    /**
     * @var Book $model
     */
    protected Book $model;

    /**
     * BookRepository constructor.
     * @param Book $model
     */
    public function __construct(Book $model)
    {
        $this->model = $model;
    }

    public function model(): Model
    {
        return $this->model;
    }

    /**
     * @param int $id
     * @param bool $withs
     * @return object|null
     */
    public function show(int $id, bool $withs = true): ?object
    {
        try {
            $query = $this->model()->newQuery();

            if ($withs) {
                $query->with(['authors']);
            }

            return $query->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            return null;
        }
    }

    /**
     * @param array $data
     * @return object|null
     */
    public function store(array $data): ?object
    {
        try {
            return $this->model()->newQuery()->create($data);
        } catch (\Exception $exception) {
            return null;
        }
    }

    /**
     * @param int $id
     * @param array $data
     * @return object|null
     */
    public function update(int $id, array $data): ?object
    {
        $this->model()->newQuery()->where('id', $id)->update($data);
        return $this->show($id, false);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function destroy(int $id)
    {
        return $this->model()->newQuery()->where('id', $id)->delete();
    }

    /**
     * @param array $conditions
     * @return object
     */
    public function list(array $conditions = []): object
    {
        $query = $this->model()->newQuery()->with(['authors']);

        if (array_key_exists('q', $conditions)) {
            $query->where(DB::raw('LOWER(title)'), 'LIKE', '%' . $conditions['q'] . '%');
        }

        return $query->paginate(20);
    }
}
